package com.droidrank.sample;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class MainActivity extends AppCompatActivity {

    private Button previous, next;
    private ImageView imageView;
    public static final String url = "http://52e4a06a.ngrok.io/fetch.php?offset=";
    public static final String TAG_IMAGES = "images";
    public static final String TAG_IMAGEURL = "imageUrl";
    public static final String TAG_IMAGEDESCRIPTION = "imageDescription";
    public static final String TAG_SUCCESS = "success";
    public static final String TAG_MESSAGE = "message";
    public LinkedHashMap<String, String> imgUrls = new LinkedHashMap<>();
    public int index = 0;
    public int offset = 0;
    JSONParser jParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        /*new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });*/
        new MyAsyncTask().execute();


    }

    private void initView() {
        imageView = (ImageView) findViewById(R.id.imageview);
        previous = (Button) findViewById(R.id.previous);
        //onclick of previous button should navigate the user to previous image
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index!=0) {
                    index = index - 1;
                    Glide.with(MainActivity.this).load(imgUrls.get(new DecimalFormat("000000").format(index))).into(imageView);
                }
            }
        });
        next = (Button) findViewById(R.id.next);
        //onclick of next button should navigate the user to next image
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index != 999999 && index < imgUrls.size()-1) {
                    index = index + 1;
                    Glide.with(MainActivity.this).load(imgUrls.get(new DecimalFormat("000000").format(index))).into(imageView);
                }else{
                    offset = offset + 1;
                    new MyAsyncTask().execute();
                    index = index + 1;
                    Glide.with(MainActivity.this).load(imgUrls.get(new DecimalFormat("000000").format(index))).into(imageView);

                }
            }
        });
    }

    private void parseData(String offset) {
        JSONObject json = jParser.getJSONFromUrlByGet(url+offset);
        try {

            JSONArray images = json.getJSONArray(TAG_IMAGES);
            for (int images_i = 0; images_i < images.length(); images_i++) {
                JSONObject images_obj = images.getJSONObject(images_i);
                String str_imageUrl = images_obj.getString(TAG_IMAGEURL);
                String str_imageDescription = images_obj.getString(TAG_IMAGEDESCRIPTION);
                if (!imgUrls.containsKey(str_imageUrl))
                    imgUrls.put(str_imageDescription, str_imageUrl);
                System.out.println(str_imageDescription);
            }
            String str_success = json.getString(TAG_SUCCESS);

            String str_message = json.getString(TAG_MESSAGE);

        } catch (JSONException e) {
            System.out.println(e);
        }
    }
    // AsyncTask
    class MyAsyncTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object... params) {

            parseData(String.valueOf(offset));
            return new Object();
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            Glide.with(MainActivity.this).load(imgUrls.get(new DecimalFormat("000000").format(index))).into(imageView);
        }
    }
}
